# MyFTP
My implementation of a basic FTP server in C.

# Usage
```bash
./myftp port path
```
Where:
- `port` is the port number
- `path` is the path to set as FTP root

Example:\
```./myftp 21 .```

The default user is named `Anonymous` and requires an empty password. These credentials are customisable under the `USER` and `PASSWORD` constants in `include/common.h`.

# Author
Alex Trefilov (@volifter)
