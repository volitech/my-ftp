/*
** EPITECH PROJECT, 2020
** myftp
** File description:
** Utility functions
*/

#include <sys/select.h>

#include "common.h"

char *resolve_path(char *path, char *base_dir)
{
    char *real_path;

    if (!strcmp(path, "/"))
        return strdup(base_dir);
    if (!path)
        return NULL;
    asprintf(&path, "%s/%s", base_dir, path);
    real_path = realpath(path, NULL);
    free(path);
    if (!real_path)
        return NULL;
    return real_path;
}

FILE *open_file(connection_t *conn, char *path, char *mode)
{
    char *file_name = basename(path);
    char *real_path = resolve_path(dirname(path), conn->path);
    FILE *file      = 0;
    char *full_path;

    if (real_path) {
        asprintf(&full_path, "%s/%s", real_path, file_name);
        file = fopen(full_path, mode);
        free(full_path);
    }
    free(real_path);
    return file;
}

void wait_for_read(int fd)
{
    fd_set rfds;

    FD_ZERO(&rfds);
    FD_SET(fd, &rfds);
    select(fd + 1, &rfds, NULL, NULL, NULL);
}

void wait_for_write(int fd)
{
    fd_set rfds;

    FD_ZERO(&rfds);
    FD_SET(fd, &rfds);
    select(fd + 1, NULL, &rfds, NULL, NULL);
}

char *escape_shell_arg(char *str)
{
    size_t len  = strlen(str);
    char *ret   = malloc(sizeof(char) * (len * 2 + 1));
    char *start = ret;

    for (; *str; str++) {
        if (*str == '"' || *str == '\\')
            *ret++ = '\\';
        *ret++ = *str;
    }
    *ret = 0;
    return start;
}
