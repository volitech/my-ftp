/*
** EPITECH PROJECT, 2020
** myftp
** File description:
** Commands relative to the file system
*/

#include "common.h"

static bool set_dir(connection_t *conn, char *dir, bool is_relative)
{
    char *path = resolve_path(dir, is_relative ? conn->cwd : conn->path);

    if (!path)
        return call_cmd(conn, 550, "Failed to change directory.");
    if (strncmp(path, conn->path, strlen(conn->path)))
        path = strdup(conn->path);
    free(conn->cwd);
    conn->cwd = path;
    return call_cmd(conn, 250, "Requested file action okay, completed.");
}

bool cmd_cwd(connection_t *conn)
{
    if (!conn->arg || !*conn->arg)
        return call_cmd(conn, 550, "Failed to change directory.");
    return set_dir(conn, conn->arg, conn->arg && *conn->arg == '.');
}

bool cmd_cdup(connection_t *conn)
{
    return set_dir(conn, "..", true);
}

bool cmd_pwd(connection_t *conn)
{
    char *path = conn->cwd + strlen(conn->path);
    char *result;

    asprintf(&result, "\"%s\"", *path ? path : "/");
    call_cmd(conn, 257, result);
    free(result);
    return true;
}

bool cmd_dele(connection_t *conn)
{
    char *path = resolve_path(conn->arg, conn->path);

    if (!conn->arg)
        return call_cmd(conn, 501, "Syntax error.");
    if (!path)
        return call_cmd(conn, 550, "No such file.");
    if (remove(path) == -1)
        call_cmd(conn, 500, "Failed to remove file.");
    else
        call_cmd(conn, 250, "Requested file action okay, completed.");
    free(path);
    return true;
}
