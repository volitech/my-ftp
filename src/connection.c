/*
** EPITECH PROJECT, 2020
** myftp
** File description:
** Connections handling
*/

#include "common.h"

static bool set_command_id(connection_t *conn, char cmd_name[CMD_SIZE])
{
    conn->cmd_id = -1;
    for (size_t i = 0; i < sizeof(COMMANDS) / sizeof(pair_t); i++) {
        if (!strcmp(cmd_name, COMMANDS[i].name)) {
            conn->cmd_id = i;
            return true;
        }
    }
    return true;
}

static bool get_cmd(connection_t *conn)
{
    int dup_fd  = dup(conn->sock_fd);
    FILE *fd    = fdopen(dup_fd, "r");
    char *line  = NULL;
    char cmd_name[CMD_SIZE];
    size_t size;

    wait_for_read(dup_fd);
    if (!fd || getline(&line, &size, fd) == -1 || strlen(line) <= 2)
        return false;
    fclose(fd);
    close(dup_fd);
    conn->arg = malloc(sizeof(char) * strlen(line));
    conn->arg[0] = 0;
    if (!sscanf(line, "%s %[^\r\n]\r\n", cmd_name, conn->arg)) {
        free(line);
        return true;
    }
    free(line);
    return set_command_id(conn, cmd_name);
}

bool call_cmd(connection_t *conn, int code, char *str)
{
    char *response;

    asprintf(&response, "%d %s\r\n", code, str);
    wait_for_write(conn->sock_fd);
    write(conn->sock_fd, response, strlen(response));
    free(response);
    return code <= 300;
}

static bool execute_cmd(connection_t *conn)
{
    if (conn->cmd_id == -1)
        return call_cmd(conn, 500, "Invalid command.");
    return conn->is_logged_in || conn->cmd_id <= 2
        ? COMMANDS[conn->cmd_id].command(conn)
        : call_cmd(conn, 530, "Not logged in.");
}

void handle_client(connection_t *conn)
{
    conn->mode          = NOT_SELECTED;
    conn->transfer_fd   = 0;
    conn->passive_fd    = 0;
    conn->is_user_sent  = false;
    conn->is_logged_in  = false;
    conn->is_active     = true;
    conn->cwd           = strdup(conn->path);
    call_cmd(conn, 220, "Service ready for new user.");
    while (conn->is_active) {
        if (get_cmd(conn)) {
            execute_cmd(conn);
            if (conn->arg)
                free(conn->arg);
        } else
            conn->is_active = false;
    }
    printf("Connection terminated\n");
    close(conn->sock_fd);
}
