/*
** EPITECH PROJECT, 2020
** myftp
** File description:
** Miscellaneous commands
*/

#include "common.h"

bool cmd_quit(connection_t *connection)
{
  connection->is_active = false;
  return call_cmd(connection, 221, "Service closing control connection.");
}

bool cmd_help(connection_t *connection)
{
  return call_cmd(connection, 214, HELP_STR);
}

bool cmd_noop(connection_t *connection)
{
  return call_cmd(connection, 200, "Command okay.");
}
