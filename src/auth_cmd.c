/*
** EPITECH PROJECT, 2020
** myftp
** File description:
** Commands relative to authentication
*/

#include "common.h"

bool cmd_user(connection_t *connection)
{
    if (connection->is_logged_in)
        return call_cmd(connection, 230, "User logged in, proceed.");
    if (!connection->arg || strcmp(USER, connection->arg))
        return call_cmd(connection, 430, "Invalid username.");
    connection->is_user_sent = true;
    return call_cmd(connection, 331, "User name okay, need password.");
}

bool cmd_pass(connection_t *connection)
{
    if (!connection->is_user_sent)
        return call_cmd(connection, 503, "Please send USER first.");
    if (!connection->arg || strcmp(PASSWORD, connection->arg))
        return call_cmd(connection, 430, "Empty password.");
    connection->is_logged_in = true;
    return call_cmd(connection, 230, "User logged in, proceed.");
}
