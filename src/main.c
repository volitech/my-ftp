/*
** EPITECH PROJECT, 2020
** myftp
** File description:
** Main function
*/

#include <signal.h>
#include <arpa/inet.h>
#include <errno.h>

#include "common.h"

static size_t get_socket_fd(size_t new_fd)
{
    static size_t fd;

    if (new_fd)
        fd = new_fd;
    return fd;
}

void on_sigint(int signal)
{
    printf("Shutting down...\n");
    shutdown(get_socket_fd(0), SHUT_RDWR);
    exit(130);
}

static void handle_clients(connection_t *connection,
    struct sockaddr_in *address)
{
    size_t fd       = get_socket_fd(0);
    socklen_t size  = sizeof(struct sockaddr_in);

    connection->sock_fd = accept_connection(fd);
    for (; connection->sock_fd; connection->sock_fd = accept_connection(fd)) {
        if (!fork()) {
            signal(SIGINT, SIG_DFL);
            getsockname(connection->sock_fd, address, &size);
            connection->ip_addr = address->sin_addr.s_addr;
            handle_client(connection);
            printf("Client disconnected\n");
            exit(0);
        }
        close(connection->sock_fd);
    }
}

static bool init(int port, char *path)
{
    struct sockaddr_in address;
    connection_t connection;
    size_t fd = open_new_socket(&address, port);

    if (fd == -1)
        return false;
    get_socket_fd(fd);
    listen(fd, QUEUE_SIZE);
    connection.path = realpath(path, NULL);
    if (connection.path == NULL)
        return false;
    printf("=== Started MyFTP server ===\n");
    printf("Port:   %d\n", port);
    printf("Path:   %s\n", connection.path);
    printf("User:   %s\n", USER);
    signal(SIGINT, on_sigint);
    handle_clients(&connection, &address);
    return true;
}

int main(int argc, char **argv)
{
    int port;

    if (argc != 3)
        return (printf("USAGE: %s port path\n", argv[0]) && 0) + 84;
    if (access(argv[2], F_OK) == -1)
        return (fprintf(stderr, "Error: can't access %s\n", argv[2]) && 0) + 84;
    port = atoi(argv[1]);
    if (port <= 0 || port > 65535)
        return (fprintf(stderr, "Error: invalid port\n") && 0) + 84;
    if (!init(port, argv[2]))
        return (fprintf(stderr, "Error: %s\n", strerror(errno)) && 0) + 84;
    return 0;
}
