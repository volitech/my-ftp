/*
** EPITECH PROJECT, 2020
** myftp
** File description:
** Commands relative to file transfer modes
*/

#include <errno.h>
#include <arpa/inet.h>

#include "common.h"

bool cmd_pasv(connection_t *conn)
{
    socklen_t           len = sizeof(struct sockaddr_in);
    char                ret[BUFF_SIZE];
    struct sockaddr_in  address;
    uint16_t            port;

    conn->passive_fd = open_new_socket(&address, 0);
    if (conn->passive_fd == -1)
        return call_cmd(conn, 500, "Failed to open socket.");
    listen(conn->passive_fd, QUEUE_SIZE);
    getsockname(conn->passive_fd, &address, &len);
    port = ntohs(address.sin_port);
    sprintf(ret, "Entering Passive Mode (%d,%d,%d,%d,%d,%d).",
        conn->ip_addr & 0xff,
        conn->ip_addr >> 8 & 0xff,
        conn->ip_addr >> 16 & 0xff,
        conn->ip_addr >> 24 & 0xff,
        port >> 8, port & 0xff);
    conn->mode = PASSIVE;
    return call_cmd(conn, 227, ret);
}

static size_t parts_to_number(unsigned short *parts, size_t len)
{
    size_t ip = 0;

    for (size_t i = 0; i < len; i++) {
        ip <<= 8;
        ip |= parts[i] & 0xff;
    }
    return ip;
}

bool cmd_port(connection_t *conn)
{
    struct sockaddr_in  socket;
    unsigned short      ip_parts[4];
    unsigned short      port_parts[2];
    size_t              ip;
    unsigned short      port;
    char                *arg = strdup(conn->arg);

    sscanf(arg, "%hu,%hu,%hu,%hu,%hu,%hu", &ip_parts[0], &ip_parts[1],
        &ip_parts[2], &ip_parts[3], &port_parts[0], &port_parts[1]);
    free(arg);
    ip      = parts_to_number(ip_parts, 4);
    port    = parts_to_number(port_parts, 2);
    printf("Connecting to socket on ip %ul, port %d\n", ip, port);
    conn->transfer_fd = init_socket(&socket, htonl(ip), port);
    if (!conn->transfer_fd)
        return call_cmd(conn, 500, "Failed to open socket.");
    conn->mode = PORT;
    return call_cmd(conn, 200, "Command okay.");
}
