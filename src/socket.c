/*
** EPITECH PROJECT, 2020
** myftp
** File description:
** Socket management
*/

#include <arpa/inet.h>

#include "common.h"

static int open_socket_fd(struct sockaddr_in *address, u_int32_t ip,
    unsigned short port)
{
    int fd      = socket(AF_INET, SOCK_STREAM, 0);
    int optval  = 1;

    if (fd == -1
        || setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(int)) < 0)
            return 0;
    address->sin_family         = AF_INET;
    address->sin_port           = htons(port);
    address->sin_addr.s_addr    = ip;
    return fd;
}

size_t open_new_socket(struct sockaddr_in *address, unsigned short port)
{
    int fd = open_socket_fd(address, INADDR_ANY, port);

    if (bind(fd, address, sizeof(struct sockaddr_in)) < 0)
        return 0;
    return fd;
}

size_t accept_connection(int socket_fd)
{
    struct sockaddr_in address;
    socklen_t size  = sizeof(struct sockaddr_in);
    int fd          = accept(socket_fd, &address, &size);

    if (fd == -1)
        return 0;
    printf("Incoming connection from %s\n", inet_ntoa(address.sin_addr));
    return fd;
}

int init_socket(struct sockaddr_in *address, size_t ip, unsigned short port)
{
    int fd = open_socket_fd(address, ip, port);

    printf("Connecting to %s, port %d\n", inet_ntoa(address->sin_addr), port);
    if (connect(fd, address, sizeof(struct sockaddr_in)) < 0)
        return 0;
    return fd;
}
