/*
** EPITECH PROJECT, 2020
** myftp
** File description:
** Commands relative to data transfer management
*/

#include <errno.h>
#include <sys/socket.h>

#include "common.h"

static int get_data_fd(connection_t *conn)
{
    return conn->mode == PASSIVE ? accept_connection(conn->passive_fd)
        : conn->transfer_fd;
}

bool cmd_retr(connection_t *conn)
{
    FILE *file = open_file(conn, conn->arg, "rb");
    size_t fd;
    size_t read_size;
    char buf[BUFF_SIZE];

    if (!conn->mode)
        return call_cmd(conn, 425, "Use PORT or PASV first.");
    if (!conn->arg || !file)
        return call_cmd(conn, 550, "Failed to open file.");
    fd = get_data_fd(conn);
    call_cmd(conn, 150, "File status okay; about to open data connection.");
    wait_for_write(fd);
    while ((read_size = fread(buf, sizeof(char), BUFF_SIZE, file)) > 0) {
        if (write(fd, buf, read_size) < 0)
            return call_cmd(conn, 426, "Aborted");
    }
    fclose(file);
    close(fd);
    return call_cmd(conn, 226, "Closing data connection.");
}

bool cmd_stor(connection_t *conn)
{
    FILE *file = open_file(conn, conn->arg, "wb");
    size_t fd;
    size_t read_size;
    char buf[BUFF_SIZE];

    if (!conn->mode)
        return call_cmd(conn, 425, "Use PORT or PASV first.");
    if (!conn->arg || !file)
        return call_cmd(conn, 501, "Failed to open file.");
    fd = get_data_fd(conn);
    call_cmd(conn, 150, "File status okay; about to open data connection.");
    wait_for_read(fd);
    while ((read_size = read(fd, buf, BUFF_SIZE)) > 0) {
        if (fwrite(buf, sizeof(char), read_size, file) < 0)
            return call_cmd(conn, 426, "Aborted");
    }
    close(fd);
    fclose(file);
    return call_cmd(conn, 226, "Closing data connection.");
}

static FILE *popen_ls_command(connection_t *conn, char *dir)
{
    char *real_path = resolve_path(dir, conn->cwd);
    char *command;
    FILE *fd;

    asprintf(&command, "/bin/ls -l \"%s\"", escape_shell_arg(real_path));
    fd = popen(command, "r");
    free(command);
    return fd;
}

bool cmd_list(connection_t *conn)
{
    size_t fd;
    char buffer[BUFF_SIZE];
    FILE *stream;
    size_t read_size;

    if (!conn->mode)
        return call_cmd(conn, 425, "Use PORT or PASV first.");
    fd = get_data_fd(conn);
    stream = popen_ls_command(conn, "/");
    call_cmd(conn, 150, "File status okay; about to open data connection.");
    wait_for_write(fd);
    while ((read_size = fread(buffer, sizeof(char), BUFF_SIZE, stream)) > 0) {
        if (write(fd, buffer, read_size) < 0)
            return call_cmd(conn, 426, "Aborted");
    }
    close(fd);
    return call_cmd(conn, 226, "Closing data connection.");
}
