/*
** EPITECH PROJECT, 2020
** myftp
** File description:
** Common header
*/

#pragma once

#include <stdbool.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <libgen.h>

#include "structures.h"
#include "prototypes.h"

static const size_t CMD_SIZE    = 8;
static const size_t QUEUE_SIZE  = 16;
static const size_t BUFF_SIZE   = 1024;

static const char USER[] = "Anonymous";
static const char PASSWORD[] = "";
static const char HELP_STR[] = "Available comands: "
    "USER\tPASS\tCWD\tCDUP\tQUIT\tDELE\tPWD\n"
    "PORT\tPASV\tHELP\tNOOP\tRETR\tSTOR\nLIST";

static const pair_t COMMANDS[] = {
    (pair_t) {"USER",   cmd_user},
    (pair_t) {"PASS",   cmd_pass},
    (pair_t) {"CWD",    cmd_cwd},
    (pair_t) {"CDUP",   cmd_cdup},
    (pair_t) {"QUIT",   cmd_quit},
    (pair_t) {"PWD",    cmd_pwd},
    (pair_t) {"PORT",   cmd_port},
    (pair_t) {"PASV",   cmd_pasv},
    (pair_t) {"STOR",   cmd_stor},
    (pair_t) {"RETR",   cmd_retr},
    (pair_t) {"LIST",   cmd_list},
    (pair_t) {"DELE",   cmd_dele},
    (pair_t) {"HELP",   cmd_help},
    (pair_t) {"NOOP",   cmd_noop},
    (pair_t) {"TYPE",   cmd_noop}
};
