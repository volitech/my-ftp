/*
** EPITECH PROJECT, 2020
** my_ftp
** File description:
** Structures
*/

#pragma once

enum ConnectionMode {
    NOT_SELECTED    = 0,
    PASSIVE         = 1,
    PORT            = 2
};

typedef struct {
    size_t  sock_fd;
    size_t  transfer_fd;
    size_t  passive_fd;
    size_t  ip_addr;
    size_t  cmd_id;
    size_t  mode;
    char    *arg;
    char    *path;
    char    *cwd;
    bool    is_user_sent;
    bool    is_logged_in;
    bool    is_active;
} connection_t;

typedef struct {
    char *name;
    bool (*command)(connection_t *);
} pair_t;
