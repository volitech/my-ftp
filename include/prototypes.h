/*
** EPITECH PROJECT, 2020
** my_ftp
** File description:
** Functions' prototypes
*/

#ifndef PROTOTYPES_H
#define PROTOTYPES_H

// auth_cmd.c
bool cmd_user(connection_t *connection);
bool cmd_pass(connection_t *connection);

// connection.c
bool call_cmd(connection_t *conn, int code, char *str);
void handle_client(connection_t *conn);

// data_transfer_cmd.c
bool cmd_retr(connection_t *conn);
bool cmd_stor(connection_t *conn);
bool cmd_list(connection_t *conn);

// filesystem_cmd.c
bool cmd_cwd(connection_t *conn);
bool cmd_cdup(connection_t *conn);
bool cmd_pwd(connection_t *conn);
bool cmd_dele(connection_t *conn);

// main.c
void on_sigint(int signal);

// misc_cmd.c
bool cmd_quit(connection_t *connection);
bool cmd_help(connection_t *connection);
bool cmd_noop(connection_t *connection);

// socket.c
size_t open_new_socket(struct sockaddr_in *address, unsigned short port);
size_t accept_connection(int socket_fd);
int init_socket(struct sockaddr_in *address, size_t ip, unsigned short port);

// transfer_modes_cmd.c
bool cmd_pasv(connection_t *conn);
bool cmd_port(connection_t *conn);

// utils.c
char *resolve_path(char *path, char *base_dir);
FILE *open_file(connection_t *conn, char *path, char *mode);
void wait_for_read(int fd);
void wait_for_write(int fd);
char *escape_shell_arg(char *str);

#endif
