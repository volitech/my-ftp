#!/bin/bash

if [ "$#" -ne 2 ]; then
    echo "USAGE: $0 host port"
    exit 1
fi

USERNAME="Anonymous"
PASS=""

EXECUTABLE="myftp"
HOST=$1
PORT=$2
TOTAL=0
PASSED=0
MKFIFO=`which mkfifo`
PIPE=fifo
OUT=outfile
TAIL=`which tail`
NC="`which nc` -C"
TIMEOUT=1 # max time before reading server response

getcode()
{
    sleep $TIMEOUT
    local code=$1
    local data=`$TAIL -n 1 $OUT | cat -e | grep "^$code.*[$]$" | wc -l`
    return $data
}

print_failed()
{
    echo "KO"
    echo "    Test failed"
    echo "    Expected reply-code: $1"
    echo "    Received: \""`$TAIL -n 1 $OUT| cat -e`"\""
}

launch_client()
{
    local host=$1
    local port=$2

    rm -f $PIPE
    $MKFIFO $PIPE
    ($TAIL -f $PIPE 2>/dev/null | $NC $host $port &> $OUT &) >/dev/null 2>/dev/null

    echo "  Connecting to $host:$port"
    sleep $TIMEOUT
    getcode 220
    if [[ $? -eq 1 ]]; then
        return 1
    else
        echo "KO"
        echo "  Connection to $host:$port failed"
        echo "    Expected reply-code: 220"
        echo "    Received: \""`tail -n 1 $OUT | cat -e`"\""
        kill_client 2>&1 >/dev/null
        return 0
    fi
}

launch_test()
{
  local cmd=$1
  local code=$2
  TOTAL=$(($TOTAL + 1))

  printf "  Sending \"$cmd\"... "
  echo "$cmd" >$PIPE
  getcode $code
  if [[ ! $? -eq 1 ]]; then
    print_failed "$code"
    kill_client
    cleanup
    return
  fi
  PASSED=$(($PASSED + 1))
  echo "OK"
}

kill_client()
{
  rm -f $PIPE $OUT &> /dev/null
}

cleanup()
{
  rm -f $PIPE $OUT log &>/dev/null
}

# Simple authentication with USER + PASS command
test_authentication()
{
    echo "=== Authentication test suite ==="

    launch_client $HOST $PORT
    if [[ ! $? -eq 1 ]]; then
        return
    fi

    launch_test "USER $USERNAME" 331
    launch_test "PASS $PASS" 230

    printf "=== Authentication test suite complete ===\n\n"
    kill_client 2>&1 >/dev/null
}

test_cwd()
{
    echo "=== CWD test suite ==="

    launch_client $HOST $PORT
    if [[ ! $? -eq 1 ]]; then
        return
    fi

    launch_test "USER $USERNAME" 331
    launch_test "PASS $PASS" 230
    launch_test "CWD src" 250
    launch_test "CDUP" 250

    printf "=== CWD test suite complete ===\n\n"
    kill_client 2>&1 >/dev/null
}

test_pwd()
{
    echo "=== PWD test suite ==="

    launch_client $HOST $PORT
    if [[ ! $? -eq 1 ]]; then
      return
    fi

    launch_test "USER $USERNAME" 331
    launch_test "PASS $PASS" 230
    launch_test "PWD" 257

    printf "=== PWD test suite complete ===\n\n"
    kill_client 2>&1 >/dev/null
}

test_noop()
{
    echo "=== NOOP test suite ==="

    launch_client $HOST $PORT
    if [[ ! $? -eq 1 ]]; then
      return
    fi

    launch_test "USER $USERNAME" 331
    launch_test "PASS $PASS" 230
    launch_test "NOOP" 200

    printf "=== NOOP test suite complete ===\n\n"
    kill_client 2>&1 >/dev/null
}

test_list()
{
    echo "=== LIST test suite ==="

    launch_client $HOST $PORT
    if [[ ! $? -eq 1 ]]; then
      return
    fi

    launch_test "USER $USERNAME" 331
    launch_test "PASS $PASS" 230
    launch_test "PASV" 227
    launch_test "LIST" 227

    printf "=== LIST test suite complete ===\n\n"
    kill_client 2>&1 >/dev/null
}

./$EXECUTABLE "$PORT" . > /dev/null &

test_authentication
test_cwd
test_pwd
test_noop
test_list
cleanup

printf "\nRESULT:\n"
printf "  $PASSED / $TOTAL\n"
printf "  $(printf $(($PASSED * 10000 / $TOTAL)) | sed 's/..$/.&/') %%\n"

kill -2 $(pgrep $EXECUTABLE)

[ $PASSED != $TOTAL ] && exit 1 || exit 0
