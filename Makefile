##
## EPITECH PROJECT, 2019
## Makefile
## File description:
## Makefile for myftp
##

NAME		=	myftp
CC			=	gcc
CPPFLAGS	=	-I./include -w

SRC			=	filesystem_cmd.c	\
				misc_cmd.c	\
				auth_cmd.c	\
				socket.c	\
				utils.c	\
				main.c	\
				connection.c	\
				data_transfer_cmd.c	\
				transfer_modes_cmd.c

OBJ			=	$(SRC:%.c=$(OBJ_PATH)/%.o)
OBJ_PATH	=	obj
SRC_PATH	=	src

.SILENT:

all: init $(NAME) finish

init:
	printf "Building $(NAME) on "
	TZ='Europe/Paris' date +"%x, %T" ; echo
	mkdir -p $(OBJ_PATH)

finish:
	if [ "$(compiled)" == "" ]; then \
		echo "No scripts compiled."; \
	else \
		printf "\nCompiled "; \
		printf "$(compiled)" | wc -w | tr -d "\n"; echo " script(s)."; \
	fi
	echo "Generated executable \"$(NAME)\"."

$(OBJ_PATH)/%.o: $(SRC_PATH)/%.c
	$(eval compiled += $<)
	echo "Compiling $<"
	$(CC) $(CFLAGS) $(CPPFLAGS) -c -o $@ $<

$(NAME): $(OBJ)
	$(CC) $(OBJ) -o $(NAME)

clean:
	rm -Rf obj/
	printf "%-16b Cleaned object files.\n" "[$(NAME)]:"

fclean: clean
	rm -f $(NAME)
	printf "%-16b Removed executable.\n" "[$(NAME)]:"

re:
	make fclean
	echo ""
	make all

.PHONY: all init finish clean fclean re